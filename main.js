const {City} = require('./City')
const {Capital} = require('./Capital')
const {Country} = require('./Country')

async function setWeatherForAllCities(country){
    for(let c of country.cities) {
        await c.setWeather();
        await c.setForecast();
        console.log(c);
        console.log(c.weather.forecast)
    }
}

let city1 = new City('Kharkiv')
let city2 = new City('Lviv')

let capital = new Capital('Kyiv')
capital.setAirport('Boryspil')

let country = new Country('Ukraine')
country.addCity(city1)
country.addCity(city2)
country.addCity(capital)
country.addCity(new City('Vinnytsia'))
country.addCity(new City('Dnipro'))
country.addCity(new City('Chernivtsi'))
country.addCity(new City('Odessa'))
country.addCity(new City('Zhytomyr'))
country.addCity(new City('Chernihiv'))
country.addCity(new City('Herson'))

setWeatherForAllCities(country)
.then(() => {
    country.cities.sort(
        function (a, b) { 
            return b.weather.temperature.substring(1,3) - a.weather.temperature.substring(1,3); });
        console.log(country)})
    .catch((error) => {console.log(error)});