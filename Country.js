const {City} = require('./City')

class Country{
    constructor(name) {
        this.name = name
        this.cities = []
    }

    addCity(city) {
        this.cities.splice(this.cities.lenght, 0, city)
    }

    removeCity(cityName){
        let index = this.cities.findIndex((city) => city.name === cityName)
        this.cities.splice(index, 1)
    }
    
}

module.exports = {Country}